package com.mgoulene.mymenu.web.cucumber;

import com.mgoulene.mymenu.web.MymenuwebApp;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = MymenuwebApp.class)
@WebAppConfiguration
public class CucumberTestContextConfiguration {}
