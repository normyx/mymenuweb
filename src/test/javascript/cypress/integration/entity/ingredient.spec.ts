import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Ingredient e2e test', () => {
  let startingEntitiesCount = 0;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, Cypress.env('E2E_USERNAME') || 'admin', Cypress.env('E2E_PASSWORD') || 'admin');
    });
    cy.intercept('GET', '/services/mymenums/api/ingredients*').as('entitiesRequest');
    cy.visit('');
    cy.clickOnEntityMenuItem('ingredient');
    cy.wait('@entitiesRequest').then(({ request, response }) => (startingEntitiesCount = response.body.length));
    cy.visit('/');
  });

  afterEach(() => {
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogout(oauth2Data);
    });
    cy.clearCache();
  });

  it('should load Ingredients', () => {
    cy.intercept('GET', '/services/mymenums/api/ingredients*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('ingredient');
    cy.wait('@entitiesRequest');
    cy.getEntityHeading('Ingredient').should('exist');
    if (startingEntitiesCount === 0) {
      cy.get(entityTableSelector).should('not.exist');
    } else {
      cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount);
    }
    cy.visit('/');
  });

  it('should load details Ingredient page', () => {
    cy.intercept('GET', '/services/mymenums/api/ingredients*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('ingredient');
    cy.wait('@entitiesRequest');
    if (startingEntitiesCount > 0) {
      cy.get(entityDetailsButtonSelector).first().click({ force: true });
      cy.getEntityDetailsHeading('ingredient');
      cy.get(entityDetailsBackButtonSelector).should('exist');
    }
    cy.visit('/');
  });

  it('should load create Ingredient page', () => {
    cy.intercept('GET', '/services/mymenums/api/ingredients*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('ingredient');
    cy.wait('@entitiesRequest');
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('Ingredient');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.visit('/');
  });

  it('should load edit Ingredient page', () => {
    cy.intercept('GET', '/services/mymenums/api/ingredients*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('ingredient');
    cy.wait('@entitiesRequest');
    if (startingEntitiesCount > 0) {
      cy.get(entityEditButtonSelector).first().click({ force: true });
      cy.getEntityCreateUpdateHeading('Ingredient');
      cy.get(entityCreateSaveButtonSelector).should('exist');
    }
    cy.visit('/');
  });

  it('should create an instance of Ingredient', () => {
    cy.intercept('GET', '/services/mymenums/api/ingredients*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('ingredient');
    cy.wait('@entitiesRequest').then(({ request, response }) => (startingEntitiesCount = response.body.length));
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('Ingredient');

    cy.get(`[data-cy="quantity"]`).type('92266').should('have.value', '92266');

    cy.get(`[data-cy="quantityType"]`).select('QUANTITY');

    cy.setFieldSelectToLastOfEntity('product');

    cy.get(entityCreateSaveButtonSelector).click({ force: true });
    cy.scrollTo('top', { ensureScrollable: false });
    cy.get(entityCreateSaveButtonSelector).should('not.exist');
    cy.intercept('GET', '/services/mymenums/api/ingredients*').as('entitiesRequestAfterCreate');
    cy.visit('/');
    cy.clickOnEntityMenuItem('ingredient');
    cy.wait('@entitiesRequestAfterCreate');
    cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount + 1);
    cy.visit('/');
  });

  it('should delete last instance of Ingredient', () => {
    cy.intercept('GET', '/services/mymenums/api/ingredients*').as('entitiesRequest');
    cy.intercept('DELETE', '/services/mymenums/api/ingredients/*').as('deleteEntityRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('ingredient');
    cy.wait('@entitiesRequest').then(({ request, response }) => {
      startingEntitiesCount = response.body.length;
      if (startingEntitiesCount > 0) {
        cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount);
        cy.get(entityDeleteButtonSelector).last().click({ force: true });
        cy.getEntityDeleteDialogHeading('ingredient').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest');
        cy.intercept('GET', '/services/mymenums/api/ingredients*').as('entitiesRequestAfterDelete');
        cy.visit('/');
        cy.clickOnEntityMenuItem('ingredient');
        cy.wait('@entitiesRequestAfterDelete');
        cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount - 1);
      }
      cy.visit('/');
    });
  });
});
