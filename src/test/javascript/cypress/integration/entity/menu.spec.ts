import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Menu e2e test', () => {
  let startingEntitiesCount = 0;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, Cypress.env('E2E_USERNAME') || 'admin', Cypress.env('E2E_PASSWORD') || 'admin');
    });
    cy.intercept('GET', '/services/mymenums/api/menus*').as('entitiesRequest');
    cy.visit('');
    cy.clickOnEntityMenuItem('menu');
    cy.wait('@entitiesRequest').then(({ request, response }) => (startingEntitiesCount = response.body.length));
    cy.visit('/');
  });

  afterEach(() => {
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogout(oauth2Data);
    });
    cy.clearCache();
  });

  it('should load Menus', () => {
    cy.intercept('GET', '/services/mymenums/api/menus*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('menu');
    cy.wait('@entitiesRequest');
    cy.getEntityHeading('Menu').should('exist');
    if (startingEntitiesCount === 0) {
      cy.get(entityTableSelector).should('not.exist');
    } else {
      cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount);
    }
    cy.visit('/');
  });

  it('should load details Menu page', () => {
    cy.intercept('GET', '/services/mymenums/api/menus*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('menu');
    cy.wait('@entitiesRequest');
    if (startingEntitiesCount > 0) {
      cy.get(entityDetailsButtonSelector).first().click({ force: true });
      cy.getEntityDetailsHeading('menu');
      cy.get(entityDetailsBackButtonSelector).should('exist');
    }
    cy.visit('/');
  });

  it('should load create Menu page', () => {
    cy.intercept('GET', '/services/mymenums/api/menus*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('menu');
    cy.wait('@entitiesRequest');
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('Menu');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.visit('/');
  });

  it('should load edit Menu page', () => {
    cy.intercept('GET', '/services/mymenums/api/menus*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('menu');
    cy.wait('@entitiesRequest');
    if (startingEntitiesCount > 0) {
      cy.get(entityEditButtonSelector).first().click({ force: true });
      cy.getEntityCreateUpdateHeading('Menu');
      cy.get(entityCreateSaveButtonSelector).should('exist');
    }
    cy.visit('/');
  });

  it('should create an instance of Menu', () => {
    cy.intercept('GET', '/services/mymenums/api/menus*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('menu');
    cy.wait('@entitiesRequest').then(({ request, response }) => (startingEntitiesCount = response.body.length));
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('Menu');

    cy.get(`[data-cy="date"]`).type('2021-04-01').should('have.value', '2021-04-01');

    cy.get(`[data-cy="userId"]`)
      .type('Borders Personal calculating', { force: true })
      .invoke('val')
      .should('match', new RegExp('Borders Personal calculating'));

    cy.setFieldSelectToLastOfEntity('starter');

    cy.setFieldSelectToLastOfEntity('main');

    cy.setFieldSelectToLastOfEntity('dessert');

    cy.get(entityCreateSaveButtonSelector).click({ force: true });
    cy.scrollTo('top', { ensureScrollable: false });
    cy.get(entityCreateSaveButtonSelector).should('not.exist');
    cy.intercept('GET', '/services/mymenums/api/menus*').as('entitiesRequestAfterCreate');
    cy.visit('/');
    cy.clickOnEntityMenuItem('menu');
    cy.wait('@entitiesRequestAfterCreate');
    cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount + 1);
    cy.visit('/');
  });

  it('should delete last instance of Menu', () => {
    cy.intercept('GET', '/services/mymenums/api/menus*').as('entitiesRequest');
    cy.intercept('DELETE', '/services/mymenums/api/menus/*').as('deleteEntityRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('menu');
    cy.wait('@entitiesRequest').then(({ request, response }) => {
      startingEntitiesCount = response.body.length;
      if (startingEntitiesCount > 0) {
        cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount);
        cy.get(entityDeleteButtonSelector).last().click({ force: true });
        cy.getEntityDeleteDialogHeading('menu').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest');
        cy.intercept('GET', '/services/mymenums/api/menus*').as('entitiesRequestAfterDelete');
        cy.visit('/');
        cy.clickOnEntityMenuItem('menu');
        cy.wait('@entitiesRequestAfterDelete');
        cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount - 1);
      }
      cy.visit('/');
    });
  });
});
