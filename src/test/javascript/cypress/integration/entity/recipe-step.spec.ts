import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('RecipeStep e2e test', () => {
  let startingEntitiesCount = 0;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, Cypress.env('E2E_USERNAME') || 'admin', Cypress.env('E2E_PASSWORD') || 'admin');
    });
    cy.intercept('GET', '/services/mymenums/api/recipe-steps*').as('entitiesRequest');
    cy.visit('');
    cy.clickOnEntityMenuItem('recipe-step');
    cy.wait('@entitiesRequest').then(({ request, response }) => (startingEntitiesCount = response.body.length));
    cy.visit('/');
  });

  afterEach(() => {
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogout(oauth2Data);
    });
    cy.clearCache();
  });

  it('should load RecipeSteps', () => {
    cy.intercept('GET', '/services/mymenums/api/recipe-steps*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('recipe-step');
    cy.wait('@entitiesRequest');
    cy.getEntityHeading('RecipeStep').should('exist');
    if (startingEntitiesCount === 0) {
      cy.get(entityTableSelector).should('not.exist');
    } else {
      cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount);
    }
    cy.visit('/');
  });

  it('should load details RecipeStep page', () => {
    cy.intercept('GET', '/services/mymenums/api/recipe-steps*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('recipe-step');
    cy.wait('@entitiesRequest');
    if (startingEntitiesCount > 0) {
      cy.get(entityDetailsButtonSelector).first().click({ force: true });
      cy.getEntityDetailsHeading('recipeStep');
      cy.get(entityDetailsBackButtonSelector).should('exist');
    }
    cy.visit('/');
  });

  it('should load create RecipeStep page', () => {
    cy.intercept('GET', '/services/mymenums/api/recipe-steps*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('recipe-step');
    cy.wait('@entitiesRequest');
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('RecipeStep');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.visit('/');
  });

  it('should load edit RecipeStep page', () => {
    cy.intercept('GET', '/services/mymenums/api/recipe-steps*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('recipe-step');
    cy.wait('@entitiesRequest');
    if (startingEntitiesCount > 0) {
      cy.get(entityEditButtonSelector).first().click({ force: true });
      cy.getEntityCreateUpdateHeading('RecipeStep');
      cy.get(entityCreateSaveButtonSelector).should('exist');
    }
    cy.visit('/');
  });

  it('should create an instance of RecipeStep', () => {
    cy.intercept('GET', '/services/mymenums/api/recipe-steps*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('recipe-step');
    cy.wait('@entitiesRequest').then(({ request, response }) => (startingEntitiesCount = response.body.length));
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('RecipeStep');

    cy.get(`[data-cy="stepNum"]`).type('83522').should('have.value', '83522');

    cy.get(`[data-cy="stepDescription"]`).type('Agent', { force: true }).invoke('val').should('match', new RegExp('Agent'));

    cy.get(entityCreateSaveButtonSelector).click({ force: true });
    cy.scrollTo('top', { ensureScrollable: false });
    cy.get(entityCreateSaveButtonSelector).should('not.exist');
    cy.intercept('GET', '/services/mymenums/api/recipe-steps*').as('entitiesRequestAfterCreate');
    cy.visit('/');
    cy.clickOnEntityMenuItem('recipe-step');
    cy.wait('@entitiesRequestAfterCreate');
    cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount + 1);
    cy.visit('/');
  });

  it('should delete last instance of RecipeStep', () => {
    cy.intercept('GET', '/services/mymenums/api/recipe-steps*').as('entitiesRequest');
    cy.intercept('DELETE', '/services/mymenums/api/recipe-steps/*').as('deleteEntityRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('recipe-step');
    cy.wait('@entitiesRequest').then(({ request, response }) => {
      startingEntitiesCount = response.body.length;
      if (startingEntitiesCount > 0) {
        cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount);
        cy.get(entityDeleteButtonSelector).last().click({ force: true });
        cy.getEntityDeleteDialogHeading('recipeStep').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest');
        cy.intercept('GET', '/services/mymenums/api/recipe-steps*').as('entitiesRequestAfterDelete');
        cy.visit('/');
        cy.clickOnEntityMenuItem('recipe-step');
        cy.wait('@entitiesRequestAfterDelete');
        cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount - 1);
      }
      cy.visit('/');
    });
  });
});
