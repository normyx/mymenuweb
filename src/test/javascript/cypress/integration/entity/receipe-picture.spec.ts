import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('ReceipePicture e2e test', () => {
  let startingEntitiesCount = 0;

  beforeEach(() => {
    cy.getOauth2Data();
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogin(oauth2Data, Cypress.env('E2E_USERNAME') || 'admin', Cypress.env('E2E_PASSWORD') || 'admin');
    });
    cy.intercept('GET', '/services/mymenums/api/receipe-pictures*').as('entitiesRequest');
    cy.visit('');
    cy.clickOnEntityMenuItem('receipe-picture');
    cy.wait('@entitiesRequest').then(({ request, response }) => (startingEntitiesCount = response.body.length));
    cy.visit('/');
  });

  afterEach(() => {
    cy.get('@oauth2Data').then(oauth2Data => {
      cy.oauthLogout(oauth2Data);
    });
    cy.clearCache();
  });

  it('should load ReceipePictures', () => {
    cy.intercept('GET', '/services/mymenums/api/receipe-pictures*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('receipe-picture');
    cy.wait('@entitiesRequest');
    cy.getEntityHeading('ReceipePicture').should('exist');
    if (startingEntitiesCount === 0) {
      cy.get(entityTableSelector).should('not.exist');
    } else {
      cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount);
    }
    cy.visit('/');
  });

  it('should load details ReceipePicture page', () => {
    cy.intercept('GET', '/services/mymenums/api/receipe-pictures*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('receipe-picture');
    cy.wait('@entitiesRequest');
    if (startingEntitiesCount > 0) {
      cy.get(entityDetailsButtonSelector).first().click({ force: true });
      cy.getEntityDetailsHeading('receipePicture');
      cy.get(entityDetailsBackButtonSelector).should('exist');
    }
    cy.visit('/');
  });

  it('should load create ReceipePicture page', () => {
    cy.intercept('GET', '/services/mymenums/api/receipe-pictures*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('receipe-picture');
    cy.wait('@entitiesRequest');
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('ReceipePicture');
    cy.get(entityCreateSaveButtonSelector).should('exist');
    cy.visit('/');
  });

  it('should load edit ReceipePicture page', () => {
    cy.intercept('GET', '/services/mymenums/api/receipe-pictures*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('receipe-picture');
    cy.wait('@entitiesRequest');
    if (startingEntitiesCount > 0) {
      cy.get(entityEditButtonSelector).first().click({ force: true });
      cy.getEntityCreateUpdateHeading('ReceipePicture');
      cy.get(entityCreateSaveButtonSelector).should('exist');
    }
    cy.visit('/');
  });

  it('should create an instance of ReceipePicture', () => {
    cy.intercept('GET', '/services/mymenums/api/receipe-pictures*').as('entitiesRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('receipe-picture');
    cy.wait('@entitiesRequest').then(({ request, response }) => (startingEntitiesCount = response.body.length));
    cy.get(entityCreateButtonSelector).click({ force: true });
    cy.getEntityCreateUpdateHeading('ReceipePicture');

    cy.setFieldImageAsBytesOfEntity('picture', 'integration-test.png', 'image/png');

    cy.get(`[data-cy="label"]`).type('Shirt Fantastic', { force: true }).invoke('val').should('match', new RegExp('Shirt Fantastic'));

    cy.get(`[data-cy="order"]`).type('36191').should('have.value', '36191');

    cy.get(entityCreateSaveButtonSelector).click({ force: true });
    cy.scrollTo('top', { ensureScrollable: false });
    cy.get(entityCreateSaveButtonSelector).should('not.exist');
    cy.intercept('GET', '/services/mymenums/api/receipe-pictures*').as('entitiesRequestAfterCreate');
    cy.visit('/');
    cy.clickOnEntityMenuItem('receipe-picture');
    cy.wait('@entitiesRequestAfterCreate');
    cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount + 1);
    cy.visit('/');
  });

  it('should delete last instance of ReceipePicture', () => {
    cy.intercept('GET', '/services/mymenums/api/receipe-pictures*').as('entitiesRequest');
    cy.intercept('DELETE', '/services/mymenums/api/receipe-pictures/*').as('deleteEntityRequest');
    cy.visit('/');
    cy.clickOnEntityMenuItem('receipe-picture');
    cy.wait('@entitiesRequest').then(({ request, response }) => {
      startingEntitiesCount = response.body.length;
      if (startingEntitiesCount > 0) {
        cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount);
        cy.get(entityDeleteButtonSelector).last().click({ force: true });
        cy.getEntityDeleteDialogHeading('receipePicture').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest');
        cy.intercept('GET', '/services/mymenums/api/receipe-pictures*').as('entitiesRequestAfterDelete');
        cy.visit('/');
        cy.clickOnEntityMenuItem('receipe-picture');
        cy.wait('@entitiesRequestAfterDelete');
        cy.get(entityTableSelector).should('have.lengthOf', startingEntitiesCount - 1);
      }
      cy.visit('/');
    });
  });
});
