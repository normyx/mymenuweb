import { NgModule } from '@angular/core';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { SharedModule } from 'app/shared/shared.module';
import { MMRecipeDetailComponent } from './detail/mm-recipe-detail.component';
import { MMRecipeRoutingModule } from './route/mm-recipe-routing.module';


@NgModule({
  imports: [SharedModule, IvyCarouselModule, MMRecipeRoutingModule],
  declarations: [MMRecipeDetailComponent],
  entryComponents: [],
})
export class MymenumsMMRecipeModule {}
