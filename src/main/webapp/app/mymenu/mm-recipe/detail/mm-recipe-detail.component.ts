import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IReceipePicture } from 'app/entities/mymenums/receipe-picture/receipe-picture.model';
import { ReceipePictureService } from 'app/entities/mymenums/receipe-picture/service/receipe-picture.service';
import { IRecipe } from 'app/entities/mymenums/recipe/recipe.model';



@Component({
  selector: 'jhi-mm-recipe-detail',
  templateUrl: './mm-recipe-detail.component.html',
})
export class MMRecipeDetailComponent implements OnInit {
  recipe: IRecipe | null = null;
  recipePicture : IReceipePicture | null = null;

  constructor(protected activatedRoute: ActivatedRoute, protected recipePictureService: ReceipePictureService) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recipe }) => {
      this.recipe = recipe;
    
    });
  }

  previousState(): void {
    window.history.back();
  }

  totalDuration(): number {
    let totalDuration = 0;
    if (this.recipe?.cookingDuration) {
      totalDuration += this.recipe.cookingDuration;
    }
    if (this.recipe?.preparationDuration) {
      totalDuration += this.recipe.preparationDuration;
    }
    if (this.recipe?.restDuraction) {
      totalDuration += this.recipe.restDuraction;
    }
    return totalDuration;
  }
}
