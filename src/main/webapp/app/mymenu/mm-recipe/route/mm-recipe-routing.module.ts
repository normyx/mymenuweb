import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MMRecipeDetailComponent } from '../detail/mm-recipe-detail.component';
import { MMRecipeRoutingResolveService } from './mm-recipe-routing-resolve.service';


const recipeRoute: Routes = [
  
  {
    path: ':id/view',
    component: MMRecipeDetailComponent,
    resolve: {
      recipe: MMRecipeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(recipeRoute)],
  exports: [RouterModule],
})
export class MMRecipeRoutingModule {}
