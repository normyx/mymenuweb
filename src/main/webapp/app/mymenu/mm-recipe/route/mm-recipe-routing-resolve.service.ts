import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { IRecipe, Recipe } from 'app/entities/mymenums/recipe/recipe.model';
import { RecipeService } from 'app/entities/mymenums/recipe/service/recipe.service';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';



@Injectable({ providedIn: 'root' })
export class MMRecipeRoutingResolveService implements Resolve<IRecipe> {
  constructor(protected service: RecipeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRecipe> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((recipe: HttpResponse<Recipe>) => {
          if (recipe.body) {
            return of(recipe.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Recipe());
  }
}
