import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
     
      {
        path: 'mm-recipe',
        data: { pageTitle: 'mymenuwebApp.mymenumsRecipe.home.title' },
        loadChildren: () => import('./mm-recipe/mm-recipe.module').then(m => m.MymenumsMMRecipeModule),
      },

    ]),
  ],
})
export class MMRoutingModule {}
