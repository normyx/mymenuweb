import { Pipe, PipeTransform } from '@angular/core';

import * as humanizeDuration from 'humanize-duration';

@Pipe({
  name: 'cookingduration',
})
export class CookingDurationPipe implements PipeTransform {
  transform(value: any): string {
    if (value) {
      
      return humanizeDuration(value*60*1000,  { language: "fr" });
    }
    return '';
  }
}
