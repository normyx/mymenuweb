import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'ingredient',
        data: { pageTitle: 'mymenuwebApp.mymenumsIngredient.home.title' },
        loadChildren: () => import('./mymenums/ingredient/ingredient.module').then(m => m.MymenumsIngredientModule),
      },
      {
        path: 'menu',
        data: { pageTitle: 'mymenuwebApp.mymenumsMenu.home.title' },
        loadChildren: () => import('./mymenums/menu/menu.module').then(m => m.MymenumsMenuModule),
      },
      {
        path: 'product',
        data: { pageTitle: 'mymenuwebApp.mymenumsProduct.home.title' },
        loadChildren: () => import('./mymenums/product/product.module').then(m => m.MymenumsProductModule),
      },
      {
        path: 'receipe-picture',
        data: { pageTitle: 'mymenuwebApp.mymenumsReceipePicture.home.title' },
        loadChildren: () => import('./mymenums/receipe-picture/receipe-picture.module').then(m => m.MymenumsReceipePictureModule),
      },
      {
        path: 'recipe',
        data: { pageTitle: 'mymenuwebApp.mymenumsRecipe.home.title' },
        loadChildren: () => import('./mymenums/recipe/recipe.module').then(m => m.MymenumsRecipeModule),
      },
      {
        path: 'recipe-step',
        data: { pageTitle: 'mymenuwebApp.mymenumsRecipeStep.home.title' },
        loadChildren: () => import('./mymenums/recipe-step/recipe-step.module').then(m => m.MymenumsRecipeStepModule),
      },
      {
        path: 'sheld',
        data: { pageTitle: 'mymenuwebApp.mymenumsSheld.home.title' },
        loadChildren: () => import('./mymenums/sheld/sheld.module').then(m => m.MymenumsSheldModule),
      },
      {
        path: 'tag',
        data: { pageTitle: 'mymenuwebApp.mymenumsTag.home.title' },
        loadChildren: () => import('./mymenums/tag/tag.module').then(m => m.MymenumsTagModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
