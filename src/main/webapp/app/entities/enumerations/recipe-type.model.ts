export enum RecipeType {
  STARTER = 'STARTER',

  MAIN = 'MAIN',

  DESSERT = 'DESSERT',
}
