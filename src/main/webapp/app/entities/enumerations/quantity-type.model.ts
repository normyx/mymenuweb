export enum QuantityType {
  QUANTITY = 'QUANTITY',

  G = 'G',

  KG = 'KG',

  L = 'L',

  ML = 'ML',

  SOUPSPOON = 'SOUPSPOON',

  TEASPON = 'TEASPON',

  PINCH = 'PINCH',
}
