import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IReceipePicture } from '../receipe-picture.model';
import { ReceipePictureService } from '../service/receipe-picture.service';

@Component({
  templateUrl: './receipe-picture-delete-dialog.component.html',
})
export class ReceipePictureDeleteDialogComponent {
  receipePicture?: IReceipePicture;

  constructor(protected receipePictureService: ReceipePictureService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.receipePictureService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
