import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IReceipePicture, ReceipePicture } from '../receipe-picture.model';

import { ReceipePictureService } from './receipe-picture.service';

describe('Service Tests', () => {
  describe('ReceipePicture Service', () => {
    let service: ReceipePictureService;
    let httpMock: HttpTestingController;
    let elemDefault: IReceipePicture;
    let expectedResult: IReceipePicture | IReceipePicture[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ReceipePictureService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        pictureContentType: 'image/png',
        picture: 'AAAAAAA',
        label: 'AAAAAAA',
        order: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ReceipePicture', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new ReceipePicture()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ReceipePicture', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            picture: 'BBBBBB',
            label: 'BBBBBB',
            order: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a ReceipePicture', () => {
        const patchObject = Object.assign(
          {
            order: 1,
          },
          new ReceipePicture()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ReceipePicture', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            picture: 'BBBBBB',
            label: 'BBBBBB',
            order: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ReceipePicture', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addReceipePictureToCollectionIfMissing', () => {
        it('should add a ReceipePicture to an empty array', () => {
          const receipePicture: IReceipePicture = { id: 123 };
          expectedResult = service.addReceipePictureToCollectionIfMissing([], receipePicture);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(receipePicture);
        });

        it('should not add a ReceipePicture to an array that contains it', () => {
          const receipePicture: IReceipePicture = { id: 123 };
          const receipePictureCollection: IReceipePicture[] = [
            {
              ...receipePicture,
            },
            { id: 456 },
          ];
          expectedResult = service.addReceipePictureToCollectionIfMissing(receipePictureCollection, receipePicture);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a ReceipePicture to an array that doesn't contain it", () => {
          const receipePicture: IReceipePicture = { id: 123 };
          const receipePictureCollection: IReceipePicture[] = [{ id: 456 }];
          expectedResult = service.addReceipePictureToCollectionIfMissing(receipePictureCollection, receipePicture);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(receipePicture);
        });

        it('should add only unique ReceipePicture to an array', () => {
          const receipePictureArray: IReceipePicture[] = [{ id: 123 }, { id: 456 }, { id: 92100 }];
          const receipePictureCollection: IReceipePicture[] = [{ id: 123 }];
          expectedResult = service.addReceipePictureToCollectionIfMissing(receipePictureCollection, ...receipePictureArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const receipePicture: IReceipePicture = { id: 123 };
          const receipePicture2: IReceipePicture = { id: 456 };
          expectedResult = service.addReceipePictureToCollectionIfMissing([], receipePicture, receipePicture2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(receipePicture);
          expect(expectedResult).toContain(receipePicture2);
        });

        it('should accept null and undefined values', () => {
          const receipePicture: IReceipePicture = { id: 123 };
          expectedResult = service.addReceipePictureToCollectionIfMissing([], null, receipePicture, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(receipePicture);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
