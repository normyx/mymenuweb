import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IReceipePicture, getReceipePictureIdentifier } from '../receipe-picture.model';

export type EntityResponseType = HttpResponse<IReceipePicture>;
export type EntityArrayResponseType = HttpResponse<IReceipePicture[]>;

@Injectable({ providedIn: 'root' })
export class ReceipePictureService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/receipe-pictures', 'mymenums');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(receipePicture: IReceipePicture): Observable<EntityResponseType> {
    return this.http.post<IReceipePicture>(this.resourceUrl, receipePicture, { observe: 'response' });
  }

  update(receipePicture: IReceipePicture): Observable<EntityResponseType> {
    return this.http.put<IReceipePicture>(`${this.resourceUrl}/${getReceipePictureIdentifier(receipePicture) as number}`, receipePicture, {
      observe: 'response',
    });
  }

  partialUpdate(receipePicture: IReceipePicture): Observable<EntityResponseType> {
    return this.http.patch<IReceipePicture>(
      `${this.resourceUrl}/${getReceipePictureIdentifier(receipePicture) as number}`,
      receipePicture,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IReceipePicture>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IReceipePicture[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addReceipePictureToCollectionIfMissing(
    receipePictureCollection: IReceipePicture[],
    ...receipePicturesToCheck: (IReceipePicture | null | undefined)[]
  ): IReceipePicture[] {
    const receipePictures: IReceipePicture[] = receipePicturesToCheck.filter(isPresent);
    if (receipePictures.length > 0) {
      const receipePictureCollectionIdentifiers = receipePictureCollection.map(
        receipePictureItem => getReceipePictureIdentifier(receipePictureItem)!
      );
      const receipePicturesToAdd = receipePictures.filter(receipePictureItem => {
        const receipePictureIdentifier = getReceipePictureIdentifier(receipePictureItem);
        if (receipePictureIdentifier == null || receipePictureCollectionIdentifiers.includes(receipePictureIdentifier)) {
          return false;
        }
        receipePictureCollectionIdentifiers.push(receipePictureIdentifier);
        return true;
      });
      return [...receipePicturesToAdd, ...receipePictureCollection];
    }
    return receipePictureCollection;
  }
}
