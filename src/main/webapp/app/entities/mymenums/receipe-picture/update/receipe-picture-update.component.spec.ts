jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ReceipePictureService } from '../service/receipe-picture.service';
import { IReceipePicture, ReceipePicture } from '../receipe-picture.model';

import { ReceipePictureUpdateComponent } from './receipe-picture-update.component';

describe('Component Tests', () => {
  describe('ReceipePicture Management Update Component', () => {
    let comp: ReceipePictureUpdateComponent;
    let fixture: ComponentFixture<ReceipePictureUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let receipePictureService: ReceipePictureService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ReceipePictureUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(ReceipePictureUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ReceipePictureUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      receipePictureService = TestBed.inject(ReceipePictureService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const receipePicture: IReceipePicture = { id: 456 };

        activatedRoute.data = of({ receipePicture });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(receipePicture));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const receipePicture = { id: 123 };
        spyOn(receipePictureService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ receipePicture });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: receipePicture }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(receipePictureService.update).toHaveBeenCalledWith(receipePicture);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const receipePicture = new ReceipePicture();
        spyOn(receipePictureService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ receipePicture });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: receipePicture }));
        saveSubject.complete();

        // THEN
        expect(receipePictureService.create).toHaveBeenCalledWith(receipePicture);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const receipePicture = { id: 123 };
        spyOn(receipePictureService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ receipePicture });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(receipePictureService.update).toHaveBeenCalledWith(receipePicture);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
