import { IRecipe } from 'app/entities/mymenums/recipe/recipe.model';

export interface IReceipePicture {
  id?: number;
  pictureContentType?: string;
  picture?: string;
  label?: string | null;
  order?: number;
  recipes?: IRecipe[] | null;
}

export class ReceipePicture implements IReceipePicture {
  constructor(
    public id?: number,
    public pictureContentType?: string,
    public picture?: string,
    public label?: string | null,
    public order?: number,
    public recipes?: IRecipe[] | null
  ) {}
}

export function getReceipePictureIdentifier(receipePicture: IReceipePicture): number | undefined {
  return receipePicture.id;
}
