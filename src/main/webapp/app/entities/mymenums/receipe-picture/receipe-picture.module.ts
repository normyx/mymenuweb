import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { ReceipePictureComponent } from './list/receipe-picture.component';
import { ReceipePictureDetailComponent } from './detail/receipe-picture-detail.component';
import { ReceipePictureUpdateComponent } from './update/receipe-picture-update.component';
import { ReceipePictureDeleteDialogComponent } from './delete/receipe-picture-delete-dialog.component';
import { ReceipePictureRoutingModule } from './route/receipe-picture-routing.module';

@NgModule({
  imports: [SharedModule, ReceipePictureRoutingModule],
  declarations: [
    ReceipePictureComponent,
    ReceipePictureDetailComponent,
    ReceipePictureUpdateComponent,
    ReceipePictureDeleteDialogComponent,
  ],
  entryComponents: [ReceipePictureDeleteDialogComponent],
})
export class MymenumsReceipePictureModule {}
