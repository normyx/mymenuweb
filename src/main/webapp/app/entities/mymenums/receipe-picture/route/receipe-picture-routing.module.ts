import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ReceipePictureComponent } from '../list/receipe-picture.component';
import { ReceipePictureDetailComponent } from '../detail/receipe-picture-detail.component';
import { ReceipePictureUpdateComponent } from '../update/receipe-picture-update.component';
import { ReceipePictureRoutingResolveService } from './receipe-picture-routing-resolve.service';

const receipePictureRoute: Routes = [
  {
    path: '',
    component: ReceipePictureComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ReceipePictureDetailComponent,
    resolve: {
      receipePicture: ReceipePictureRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ReceipePictureUpdateComponent,
    resolve: {
      receipePicture: ReceipePictureRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ReceipePictureUpdateComponent,
    resolve: {
      receipePicture: ReceipePictureRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(receipePictureRoute)],
  exports: [RouterModule],
})
export class ReceipePictureRoutingModule {}
