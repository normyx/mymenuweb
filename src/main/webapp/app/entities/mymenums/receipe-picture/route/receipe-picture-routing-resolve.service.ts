import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IReceipePicture, ReceipePicture } from '../receipe-picture.model';
import { ReceipePictureService } from '../service/receipe-picture.service';

@Injectable({ providedIn: 'root' })
export class ReceipePictureRoutingResolveService implements Resolve<IReceipePicture> {
  constructor(protected service: ReceipePictureService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IReceipePicture> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((receipePicture: HttpResponse<ReceipePicture>) => {
          if (receipePicture.body) {
            return of(receipePicture.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ReceipePicture());
  }
}
