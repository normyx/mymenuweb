import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ISheld, getSheldIdentifier } from '../sheld.model';

export type EntityResponseType = HttpResponse<ISheld>;
export type EntityArrayResponseType = HttpResponse<ISheld[]>;

@Injectable({ providedIn: 'root' })
export class SheldService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/shelds', 'mymenums');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(sheld: ISheld): Observable<EntityResponseType> {
    return this.http.post<ISheld>(this.resourceUrl, sheld, { observe: 'response' });
  }

  update(sheld: ISheld): Observable<EntityResponseType> {
    return this.http.put<ISheld>(`${this.resourceUrl}/${getSheldIdentifier(sheld) as number}`, sheld, { observe: 'response' });
  }

  partialUpdate(sheld: ISheld): Observable<EntityResponseType> {
    return this.http.patch<ISheld>(`${this.resourceUrl}/${getSheldIdentifier(sheld) as number}`, sheld, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISheld>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISheld[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addSheldToCollectionIfMissing(sheldCollection: ISheld[], ...sheldsToCheck: (ISheld | null | undefined)[]): ISheld[] {
    const shelds: ISheld[] = sheldsToCheck.filter(isPresent);
    if (shelds.length > 0) {
      const sheldCollectionIdentifiers = sheldCollection.map(sheldItem => getSheldIdentifier(sheldItem)!);
      const sheldsToAdd = shelds.filter(sheldItem => {
        const sheldIdentifier = getSheldIdentifier(sheldItem);
        if (sheldIdentifier == null || sheldCollectionIdentifiers.includes(sheldIdentifier)) {
          return false;
        }
        sheldCollectionIdentifiers.push(sheldIdentifier);
        return true;
      });
      return [...sheldsToAdd, ...sheldCollection];
    }
    return sheldCollection;
  }
}
