import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISheld } from '../sheld.model';

import { ITEMS_PER_PAGE } from 'app/config/pagination.constants';
import { SheldService } from '../service/sheld.service';
import { SheldDeleteDialogComponent } from '../delete/sheld-delete-dialog.component';
import { ParseLinks } from 'app/core/util/parse-links.service';

@Component({
  selector: 'jhi-sheld',
  templateUrl: './sheld.component.html',
})
export class SheldComponent implements OnInit {
  shelds: ISheld[];
  isLoading = false;
  itemsPerPage: number;
  links: { [key: string]: number };
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(protected sheldService: SheldService, protected modalService: NgbModal, protected parseLinks: ParseLinks) {
    this.shelds = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.isLoading = true;

    this.sheldService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<ISheld[]>) => {
          this.isLoading = false;
          this.paginateShelds(res.body, res.headers);
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  reset(): void {
    this.page = 0;
    this.shelds = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ISheld): number {
    return item.id!;
  }

  delete(sheld: ISheld): void {
    const modalRef = this.modalService.open(SheldDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.sheld = sheld;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.reset();
      }
    });
  }

  protected sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateShelds(data: ISheld[] | null, headers: HttpHeaders): void {
    this.links = this.parseLinks.parse(headers.get('link') ?? '');
    if (data) {
      for (const d of data) {
        this.shelds.push(d);
      }
    }
  }
}
