jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ISheld, Sheld } from '../sheld.model';
import { SheldService } from '../service/sheld.service';

import { SheldRoutingResolveService } from './sheld-routing-resolve.service';

describe('Service Tests', () => {
  describe('Sheld routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: SheldRoutingResolveService;
    let service: SheldService;
    let resultSheld: ISheld | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(SheldRoutingResolveService);
      service = TestBed.inject(SheldService);
      resultSheld = undefined;
    });

    describe('resolve', () => {
      it('should return ISheld returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultSheld = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultSheld).toEqual({ id: 123 });
      });

      it('should return new ISheld if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultSheld = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultSheld).toEqual(new Sheld());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultSheld = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultSheld).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
