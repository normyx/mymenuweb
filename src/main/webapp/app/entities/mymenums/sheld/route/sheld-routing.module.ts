import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { SheldComponent } from '../list/sheld.component';
import { SheldDetailComponent } from '../detail/sheld-detail.component';
import { SheldUpdateComponent } from '../update/sheld-update.component';
import { SheldRoutingResolveService } from './sheld-routing-resolve.service';

const sheldRoute: Routes = [
  {
    path: '',
    component: SheldComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SheldDetailComponent,
    resolve: {
      sheld: SheldRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SheldUpdateComponent,
    resolve: {
      sheld: SheldRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SheldUpdateComponent,
    resolve: {
      sheld: SheldRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(sheldRoute)],
  exports: [RouterModule],
})
export class SheldRoutingModule {}
