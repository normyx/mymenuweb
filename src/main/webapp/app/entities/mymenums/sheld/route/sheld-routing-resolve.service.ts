import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ISheld, Sheld } from '../sheld.model';
import { SheldService } from '../service/sheld.service';

@Injectable({ providedIn: 'root' })
export class SheldRoutingResolveService implements Resolve<ISheld> {
  constructor(protected service: SheldService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISheld> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((sheld: HttpResponse<Sheld>) => {
          if (sheld.body) {
            return of(sheld.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Sheld());
  }
}
