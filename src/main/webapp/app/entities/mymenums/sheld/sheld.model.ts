export interface ISheld {
  id?: number;
  name?: string;
}

export class Sheld implements ISheld {
  constructor(public id?: number, public name?: string) {}
}

export function getSheldIdentifier(sheld: ISheld): number | undefined {
  return sheld.id;
}
