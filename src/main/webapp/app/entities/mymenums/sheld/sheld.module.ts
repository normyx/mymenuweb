import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { SheldComponent } from './list/sheld.component';
import { SheldDetailComponent } from './detail/sheld-detail.component';
import { SheldUpdateComponent } from './update/sheld-update.component';
import { SheldDeleteDialogComponent } from './delete/sheld-delete-dialog.component';
import { SheldRoutingModule } from './route/sheld-routing.module';

@NgModule({
  imports: [SharedModule, SheldRoutingModule],
  declarations: [SheldComponent, SheldDetailComponent, SheldUpdateComponent, SheldDeleteDialogComponent],
  entryComponents: [SheldDeleteDialogComponent],
})
export class MymenumsSheldModule {}
