jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { SheldService } from '../service/sheld.service';
import { ISheld, Sheld } from '../sheld.model';

import { SheldUpdateComponent } from './sheld-update.component';

describe('Component Tests', () => {
  describe('Sheld Management Update Component', () => {
    let comp: SheldUpdateComponent;
    let fixture: ComponentFixture<SheldUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let sheldService: SheldService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [SheldUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(SheldUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SheldUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      sheldService = TestBed.inject(SheldService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const sheld: ISheld = { id: 456 };

        activatedRoute.data = of({ sheld });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(sheld));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const sheld = { id: 123 };
        spyOn(sheldService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ sheld });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: sheld }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(sheldService.update).toHaveBeenCalledWith(sheld);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const sheld = new Sheld();
        spyOn(sheldService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ sheld });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: sheld }));
        saveSubject.complete();

        // THEN
        expect(sheldService.create).toHaveBeenCalledWith(sheld);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const sheld = { id: 123 };
        spyOn(sheldService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ sheld });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(sheldService.update).toHaveBeenCalledWith(sheld);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
