import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ISheld, Sheld } from '../sheld.model';
import { SheldService } from '../service/sheld.service';

@Component({
  selector: 'jhi-sheld-update',
  templateUrl: './sheld-update.component.html',
})
export class SheldUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(200)]],
  });

  constructor(protected sheldService: SheldService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sheld }) => {
      this.updateForm(sheld);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const sheld = this.createFromForm();
    if (sheld.id !== undefined) {
      this.subscribeToSaveResponse(this.sheldService.update(sheld));
    } else {
      this.subscribeToSaveResponse(this.sheldService.create(sheld));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISheld>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(sheld: ISheld): void {
    this.editForm.patchValue({
      id: sheld.id,
      name: sheld.name,
    });
  }

  protected createFromForm(): ISheld {
    return {
      ...new Sheld(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }
}
