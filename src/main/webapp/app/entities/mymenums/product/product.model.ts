import { ISheld } from 'app/entities/mymenums/sheld/sheld.model';

export interface IProduct {
  id?: number;
  name?: string;
  pictureContentType?: string | null;
  picture?: string | null;
  sheld?: ISheld | null;
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public name?: string,
    public pictureContentType?: string | null,
    public picture?: string | null,
    public sheld?: ISheld | null
  ) {}
}

export function getProductIdentifier(product: IProduct): number | undefined {
  return product.id;
}
