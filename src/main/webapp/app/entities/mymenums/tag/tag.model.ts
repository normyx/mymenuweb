import { IRecipe } from 'app/entities/mymenums/recipe/recipe.model';

export interface ITag {
  id?: number;
  label?: string;
  recipes?: IRecipe[] | null;
}

export class Tag implements ITag {
  constructor(public id?: number, public label?: string, public recipes?: IRecipe[] | null) {}
}

export function getTagIdentifier(tag: ITag): number | undefined {
  return tag.id;
}
