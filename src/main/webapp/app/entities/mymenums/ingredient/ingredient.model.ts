import { IProduct } from 'app/entities/mymenums/product/product.model';
import { IRecipe } from 'app/entities/mymenums/recipe/recipe.model';
import { QuantityType } from 'app/entities/enumerations/quantity-type.model';

export interface IIngredient {
  id?: number;
  quantity?: number | null;
  quantityType?: QuantityType | null;
  product?: IProduct | null;
  recipes?: IRecipe[] | null;
}

export class Ingredient implements IIngredient {
  constructor(
    public id?: number,
    public quantity?: number | null,
    public quantityType?: QuantityType | null,
    public product?: IProduct | null,
    public recipes?: IRecipe[] | null
  ) {}
}

export function getIngredientIdentifier(ingredient: IIngredient): number | undefined {
  return ingredient.id;
}
