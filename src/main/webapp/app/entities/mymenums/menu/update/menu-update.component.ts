import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IMenu, Menu } from '../menu.model';
import { MenuService } from '../service/menu.service';
import { IRecipe } from 'app/entities/mymenums/recipe/recipe.model';
import { RecipeService } from 'app/entities/mymenums/recipe/service/recipe.service';

@Component({
  selector: 'jhi-menu-update',
  templateUrl: './menu-update.component.html',
})
export class MenuUpdateComponent implements OnInit {
  isSaving = false;

  recipesSharedCollection: IRecipe[] = [];

  editForm = this.fb.group({
    id: [],
    date: [null, [Validators.required]],
    userId: [null, [Validators.required]],
    starter: [],
    main: [],
    dessert: [],
  });

  constructor(
    protected menuService: MenuService,
    protected recipeService: RecipeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ menu }) => {
      this.updateForm(menu);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const menu = this.createFromForm();
    if (menu.id !== undefined) {
      this.subscribeToSaveResponse(this.menuService.update(menu));
    } else {
      this.subscribeToSaveResponse(this.menuService.create(menu));
    }
  }

  trackRecipeById(index: number, item: IRecipe): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMenu>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(menu: IMenu): void {
    this.editForm.patchValue({
      id: menu.id,
      date: menu.date,
      userId: menu.userId,
      starter: menu.starter,
      main: menu.main,
      dessert: menu.dessert,
    });

    this.recipesSharedCollection = this.recipeService.addRecipeToCollectionIfMissing(
      this.recipesSharedCollection,
      menu.starter,
      menu.main,
      menu.dessert
    );
  }

  protected loadRelationshipsOptions(): void {
    this.recipeService
      .query()
      .pipe(map((res: HttpResponse<IRecipe[]>) => res.body ?? []))
      .pipe(
        map((recipes: IRecipe[]) =>
          this.recipeService.addRecipeToCollectionIfMissing(
            recipes,
            this.editForm.get('starter')!.value,
            this.editForm.get('main')!.value,
            this.editForm.get('dessert')!.value
          )
        )
      )
      .subscribe((recipes: IRecipe[]) => (this.recipesSharedCollection = recipes));
  }

  protected createFromForm(): IMenu {
    return {
      ...new Menu(),
      id: this.editForm.get(['id'])!.value,
      date: this.editForm.get(['date'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      starter: this.editForm.get(['starter'])!.value,
      main: this.editForm.get(['main'])!.value,
      dessert: this.editForm.get(['dessert'])!.value,
    };
  }
}
