jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { MenuService } from '../service/menu.service';
import { IMenu, Menu } from '../menu.model';
import { IRecipe } from 'app/entities/mymenums/recipe/recipe.model';
import { RecipeService } from 'app/entities/mymenums/recipe/service/recipe.service';

import { MenuUpdateComponent } from './menu-update.component';

describe('Component Tests', () => {
  describe('Menu Management Update Component', () => {
    let comp: MenuUpdateComponent;
    let fixture: ComponentFixture<MenuUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let menuService: MenuService;
    let recipeService: RecipeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [MenuUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(MenuUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MenuUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      menuService = TestBed.inject(MenuService);
      recipeService = TestBed.inject(RecipeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Recipe query and add missing value', () => {
        const menu: IMenu = { id: 456 };
        const starter: IRecipe = { id: 45946 };
        menu.starter = starter;
        const main: IRecipe = { id: 81223 };
        menu.main = main;
        const dessert: IRecipe = { id: 37538 };
        menu.dessert = dessert;

        const recipeCollection: IRecipe[] = [{ id: 30654 }];
        spyOn(recipeService, 'query').and.returnValue(of(new HttpResponse({ body: recipeCollection })));
        const additionalRecipes = [starter, main, dessert];
        const expectedCollection: IRecipe[] = [...additionalRecipes, ...recipeCollection];
        spyOn(recipeService, 'addRecipeToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ menu });
        comp.ngOnInit();

        expect(recipeService.query).toHaveBeenCalled();
        expect(recipeService.addRecipeToCollectionIfMissing).toHaveBeenCalledWith(recipeCollection, ...additionalRecipes);
        expect(comp.recipesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const menu: IMenu = { id: 456 };
        const starter: IRecipe = { id: 37752 };
        menu.starter = starter;
        const main: IRecipe = { id: 99048 };
        menu.main = main;
        const dessert: IRecipe = { id: 30814 };
        menu.dessert = dessert;

        activatedRoute.data = of({ menu });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(menu));
        expect(comp.recipesSharedCollection).toContain(starter);
        expect(comp.recipesSharedCollection).toContain(main);
        expect(comp.recipesSharedCollection).toContain(dessert);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const menu = { id: 123 };
        spyOn(menuService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ menu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: menu }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(menuService.update).toHaveBeenCalledWith(menu);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const menu = new Menu();
        spyOn(menuService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ menu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: menu }));
        saveSubject.complete();

        // THEN
        expect(menuService.create).toHaveBeenCalledWith(menu);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const menu = { id: 123 };
        spyOn(menuService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ menu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(menuService.update).toHaveBeenCalledWith(menu);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackRecipeById', () => {
        it('Should return tracked Recipe primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackRecipeById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
