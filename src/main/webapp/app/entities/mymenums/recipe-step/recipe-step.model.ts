import { IRecipe } from 'app/entities/mymenums/recipe/recipe.model';

export interface IRecipeStep {
  id?: number;
  stepNum?: number;
  stepDescription?: string | null;
  recipes?: IRecipe[] | null;
}

export class RecipeStep implements IRecipeStep {
  constructor(public id?: number, public stepNum?: number, public stepDescription?: string | null, public recipes?: IRecipe[] | null) {}
}

export function getRecipeStepIdentifier(recipeStep: IRecipeStep): number | undefined {
  return recipeStep.id;
}
