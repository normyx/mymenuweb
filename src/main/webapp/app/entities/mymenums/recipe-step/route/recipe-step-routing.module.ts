import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { RecipeStepComponent } from '../list/recipe-step.component';
import { RecipeStepDetailComponent } from '../detail/recipe-step-detail.component';
import { RecipeStepUpdateComponent } from '../update/recipe-step-update.component';
import { RecipeStepRoutingResolveService } from './recipe-step-routing-resolve.service';

const recipeStepRoute: Routes = [
  {
    path: '',
    component: RecipeStepComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: RecipeStepDetailComponent,
    resolve: {
      recipeStep: RecipeStepRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: RecipeStepUpdateComponent,
    resolve: {
      recipeStep: RecipeStepRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: RecipeStepUpdateComponent,
    resolve: {
      recipeStep: RecipeStepRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(recipeStepRoute)],
  exports: [RouterModule],
})
export class RecipeStepRoutingModule {}
