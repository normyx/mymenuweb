import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IRecipeStep, RecipeStep } from '../recipe-step.model';
import { RecipeStepService } from '../service/recipe-step.service';

@Injectable({ providedIn: 'root' })
export class RecipeStepRoutingResolveService implements Resolve<IRecipeStep> {
  constructor(protected service: RecipeStepService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRecipeStep> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((recipeStep: HttpResponse<RecipeStep>) => {
          if (recipeStep.body) {
            return of(recipeStep.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RecipeStep());
  }
}
