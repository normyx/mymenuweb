jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IRecipeStep, RecipeStep } from '../recipe-step.model';
import { RecipeStepService } from '../service/recipe-step.service';

import { RecipeStepRoutingResolveService } from './recipe-step-routing-resolve.service';

describe('Service Tests', () => {
  describe('RecipeStep routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: RecipeStepRoutingResolveService;
    let service: RecipeStepService;
    let resultRecipeStep: IRecipeStep | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(RecipeStepRoutingResolveService);
      service = TestBed.inject(RecipeStepService);
      resultRecipeStep = undefined;
    });

    describe('resolve', () => {
      it('should return IRecipeStep returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultRecipeStep = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultRecipeStep).toEqual({ id: 123 });
      });

      it('should return new IRecipeStep if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultRecipeStep = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultRecipeStep).toEqual(new RecipeStep());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultRecipeStep = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultRecipeStep).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
