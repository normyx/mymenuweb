import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRecipeStep } from '../recipe-step.model';

@Component({
  selector: 'jhi-recipe-step-detail',
  templateUrl: './recipe-step-detail.component.html',
})
export class RecipeStepDetailComponent implements OnInit {
  recipeStep: IRecipeStep | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recipeStep }) => {
      this.recipeStep = recipeStep;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
