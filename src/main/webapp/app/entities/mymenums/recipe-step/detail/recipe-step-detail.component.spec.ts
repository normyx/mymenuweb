import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RecipeStepDetailComponent } from './recipe-step-detail.component';

describe('Component Tests', () => {
  describe('RecipeStep Management Detail Component', () => {
    let comp: RecipeStepDetailComponent;
    let fixture: ComponentFixture<RecipeStepDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [RecipeStepDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ recipeStep: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(RecipeStepDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RecipeStepDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load recipeStep on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.recipeStep).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
