import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { RecipeStepComponent } from './list/recipe-step.component';
import { RecipeStepDetailComponent } from './detail/recipe-step-detail.component';
import { RecipeStepUpdateComponent } from './update/recipe-step-update.component';
import { RecipeStepDeleteDialogComponent } from './delete/recipe-step-delete-dialog.component';
import { RecipeStepRoutingModule } from './route/recipe-step-routing.module';

@NgModule({
  imports: [SharedModule, RecipeStepRoutingModule],
  declarations: [RecipeStepComponent, RecipeStepDetailComponent, RecipeStepUpdateComponent, RecipeStepDeleteDialogComponent],
  entryComponents: [RecipeStepDeleteDialogComponent],
})
export class MymenumsRecipeStepModule {}
