import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IRecipeStep, RecipeStep } from '../recipe-step.model';

import { RecipeStepService } from './recipe-step.service';

describe('Service Tests', () => {
  describe('RecipeStep Service', () => {
    let service: RecipeStepService;
    let httpMock: HttpTestingController;
    let elemDefault: IRecipeStep;
    let expectedResult: IRecipeStep | IRecipeStep[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(RecipeStepService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        stepNum: 0,
        stepDescription: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a RecipeStep', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new RecipeStep()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a RecipeStep', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            stepNum: 1,
            stepDescription: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a RecipeStep', () => {
        const patchObject = Object.assign(
          {
            stepNum: 1,
          },
          new RecipeStep()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of RecipeStep', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            stepNum: 1,
            stepDescription: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a RecipeStep', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addRecipeStepToCollectionIfMissing', () => {
        it('should add a RecipeStep to an empty array', () => {
          const recipeStep: IRecipeStep = { id: 123 };
          expectedResult = service.addRecipeStepToCollectionIfMissing([], recipeStep);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(recipeStep);
        });

        it('should not add a RecipeStep to an array that contains it', () => {
          const recipeStep: IRecipeStep = { id: 123 };
          const recipeStepCollection: IRecipeStep[] = [
            {
              ...recipeStep,
            },
            { id: 456 },
          ];
          expectedResult = service.addRecipeStepToCollectionIfMissing(recipeStepCollection, recipeStep);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a RecipeStep to an array that doesn't contain it", () => {
          const recipeStep: IRecipeStep = { id: 123 };
          const recipeStepCollection: IRecipeStep[] = [{ id: 456 }];
          expectedResult = service.addRecipeStepToCollectionIfMissing(recipeStepCollection, recipeStep);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(recipeStep);
        });

        it('should add only unique RecipeStep to an array', () => {
          const recipeStepArray: IRecipeStep[] = [{ id: 123 }, { id: 456 }, { id: 57619 }];
          const recipeStepCollection: IRecipeStep[] = [{ id: 123 }];
          expectedResult = service.addRecipeStepToCollectionIfMissing(recipeStepCollection, ...recipeStepArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const recipeStep: IRecipeStep = { id: 123 };
          const recipeStep2: IRecipeStep = { id: 456 };
          expectedResult = service.addRecipeStepToCollectionIfMissing([], recipeStep, recipeStep2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(recipeStep);
          expect(expectedResult).toContain(recipeStep2);
        });

        it('should accept null and undefined values', () => {
          const recipeStep: IRecipeStep = { id: 123 };
          expectedResult = service.addRecipeStepToCollectionIfMissing([], null, recipeStep, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(recipeStep);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
