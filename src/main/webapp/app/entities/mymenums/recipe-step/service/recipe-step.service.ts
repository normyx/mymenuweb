import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IRecipeStep, getRecipeStepIdentifier } from '../recipe-step.model';

export type EntityResponseType = HttpResponse<IRecipeStep>;
export type EntityArrayResponseType = HttpResponse<IRecipeStep[]>;

@Injectable({ providedIn: 'root' })
export class RecipeStepService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/recipe-steps', 'mymenums');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(recipeStep: IRecipeStep): Observable<EntityResponseType> {
    return this.http.post<IRecipeStep>(this.resourceUrl, recipeStep, { observe: 'response' });
  }

  update(recipeStep: IRecipeStep): Observable<EntityResponseType> {
    return this.http.put<IRecipeStep>(`${this.resourceUrl}/${getRecipeStepIdentifier(recipeStep) as number}`, recipeStep, {
      observe: 'response',
    });
  }

  partialUpdate(recipeStep: IRecipeStep): Observable<EntityResponseType> {
    return this.http.patch<IRecipeStep>(`${this.resourceUrl}/${getRecipeStepIdentifier(recipeStep) as number}`, recipeStep, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRecipeStep>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRecipeStep[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addRecipeStepToCollectionIfMissing(
    recipeStepCollection: IRecipeStep[],
    ...recipeStepsToCheck: (IRecipeStep | null | undefined)[]
  ): IRecipeStep[] {
    const recipeSteps: IRecipeStep[] = recipeStepsToCheck.filter(isPresent);
    if (recipeSteps.length > 0) {
      const recipeStepCollectionIdentifiers = recipeStepCollection.map(recipeStepItem => getRecipeStepIdentifier(recipeStepItem)!);
      const recipeStepsToAdd = recipeSteps.filter(recipeStepItem => {
        const recipeStepIdentifier = getRecipeStepIdentifier(recipeStepItem);
        if (recipeStepIdentifier == null || recipeStepCollectionIdentifiers.includes(recipeStepIdentifier)) {
          return false;
        }
        recipeStepCollectionIdentifiers.push(recipeStepIdentifier);
        return true;
      });
      return [...recipeStepsToAdd, ...recipeStepCollection];
    }
    return recipeStepCollection;
  }
}
