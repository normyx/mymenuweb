import { ITag } from 'app/entities/mymenums/tag/tag.model';
import { IIngredient } from 'app/entities/mymenums/ingredient/ingredient.model';
import { IReceipePicture } from 'app/entities/mymenums/receipe-picture/receipe-picture.model';
import { IRecipeStep } from 'app/entities/mymenums/recipe-step/recipe-step.model';
import { PreferredSeason } from 'app/entities/enumerations/preferred-season.model';
import { RecipeType } from 'app/entities/enumerations/recipe-type.model';

export interface IRecipe {
  id?: number;
  name?: string;
  description?: string | null;
  preparationDuration?: number | null;
  restDuraction?: number | null;
  cookingDuration?: number | null;
  winterPreferrency?: PreferredSeason | null;
  springPreferrency?: PreferredSeason | null;
  summerPreferrency?: PreferredSeason | null;
  autumnPreferrency?: PreferredSeason | null;
  numberOfPeople?: number | null;
  recipeType?: RecipeType;
  recipeUrl?: string | null;
  shortSteps?: string | null;
  isShortDescRecipe?: boolean;
  tags?: ITag[] | null;
  ingredients?: IIngredient[] | null;
  recipePictures?: IReceipePicture[] | null;
  recipeSteps?: IRecipeStep[] | null;
}

export class Recipe implements IRecipe {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string | null,
    public preparationDuration?: number | null,
    public restDuraction?: number | null,
    public cookingDuration?: number | null,
    public winterPreferrency?: PreferredSeason | null,
    public springPreferrency?: PreferredSeason | null,
    public summerPreferrency?: PreferredSeason | null,
    public autumnPreferrency?: PreferredSeason | null,
    public numberOfPeople?: number | null,
    public recipeType?: RecipeType,
    public recipeUrl?: string | null,
    public shortSteps?: string | null,
    public isShortDescRecipe?: boolean,
    public tags?: ITag[] | null,
    public ingredients?: IIngredient[] | null,
    public recipePictures?: IReceipePicture[] | null,
    public recipeSteps?: IRecipeStep[] | null
  ) {
    this.isShortDescRecipe = this.isShortDescRecipe ?? false;
  }
}

export function getRecipeIdentifier(recipe: IRecipe): number | undefined {
  return recipe.id;
}
