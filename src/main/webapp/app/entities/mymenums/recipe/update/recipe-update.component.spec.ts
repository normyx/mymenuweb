jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { RecipeService } from '../service/recipe.service';
import { IRecipe, Recipe } from '../recipe.model';
import { ITag } from 'app/entities/mymenums/tag/tag.model';
import { TagService } from 'app/entities/mymenums/tag/service/tag.service';
import { IIngredient } from 'app/entities/mymenums/ingredient/ingredient.model';
import { IngredientService } from 'app/entities/mymenums/ingredient/service/ingredient.service';
import { IReceipePicture } from 'app/entities/mymenums/receipe-picture/receipe-picture.model';
import { ReceipePictureService } from 'app/entities/mymenums/receipe-picture/service/receipe-picture.service';
import { IRecipeStep } from 'app/entities/mymenums/recipe-step/recipe-step.model';
import { RecipeStepService } from 'app/entities/mymenums/recipe-step/service/recipe-step.service';

import { RecipeUpdateComponent } from './recipe-update.component';

describe('Component Tests', () => {
  describe('Recipe Management Update Component', () => {
    let comp: RecipeUpdateComponent;
    let fixture: ComponentFixture<RecipeUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let recipeService: RecipeService;
    let tagService: TagService;
    let ingredientService: IngredientService;
    let receipePictureService: ReceipePictureService;
    let recipeStepService: RecipeStepService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [RecipeUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(RecipeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RecipeUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      recipeService = TestBed.inject(RecipeService);
      tagService = TestBed.inject(TagService);
      ingredientService = TestBed.inject(IngredientService);
      receipePictureService = TestBed.inject(ReceipePictureService);
      recipeStepService = TestBed.inject(RecipeStepService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Tag query and add missing value', () => {
        const recipe: IRecipe = { id: 456 };
        const tags: ITag[] = [{ id: 57486 }];
        recipe.tags = tags;

        const tagCollection: ITag[] = [{ id: 39469 }];
        spyOn(tagService, 'query').and.returnValue(of(new HttpResponse({ body: tagCollection })));
        const additionalTags = [...tags];
        const expectedCollection: ITag[] = [...additionalTags, ...tagCollection];
        spyOn(tagService, 'addTagToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ recipe });
        comp.ngOnInit();

        expect(tagService.query).toHaveBeenCalled();
        expect(tagService.addTagToCollectionIfMissing).toHaveBeenCalledWith(tagCollection, ...additionalTags);
        expect(comp.tagsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call Ingredient query and add missing value', () => {
        const recipe: IRecipe = { id: 456 };
        const ingredients: IIngredient[] = [{ id: 94300 }];
        recipe.ingredients = ingredients;

        const ingredientCollection: IIngredient[] = [{ id: 22603 }];
        spyOn(ingredientService, 'query').and.returnValue(of(new HttpResponse({ body: ingredientCollection })));
        const additionalIngredients = [...ingredients];
        const expectedCollection: IIngredient[] = [...additionalIngredients, ...ingredientCollection];
        spyOn(ingredientService, 'addIngredientToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ recipe });
        comp.ngOnInit();

        expect(ingredientService.query).toHaveBeenCalled();
        expect(ingredientService.addIngredientToCollectionIfMissing).toHaveBeenCalledWith(ingredientCollection, ...additionalIngredients);
        expect(comp.ingredientsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call ReceipePicture query and add missing value', () => {
        const recipe: IRecipe = { id: 456 };
        const recipePictures: IReceipePicture[] = [{ id: 54250 }];
        recipe.recipePictures = recipePictures;

        const receipePictureCollection: IReceipePicture[] = [{ id: 67221 }];
        spyOn(receipePictureService, 'query').and.returnValue(of(new HttpResponse({ body: receipePictureCollection })));
        const additionalReceipePictures = [...recipePictures];
        const expectedCollection: IReceipePicture[] = [...additionalReceipePictures, ...receipePictureCollection];
        spyOn(receipePictureService, 'addReceipePictureToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ recipe });
        comp.ngOnInit();

        expect(receipePictureService.query).toHaveBeenCalled();
        expect(receipePictureService.addReceipePictureToCollectionIfMissing).toHaveBeenCalledWith(
          receipePictureCollection,
          ...additionalReceipePictures
        );
        expect(comp.receipePicturesSharedCollection).toEqual(expectedCollection);
      });

      it('Should call RecipeStep query and add missing value', () => {
        const recipe: IRecipe = { id: 456 };
        const recipeSteps: IRecipeStep[] = [{ id: 42060 }];
        recipe.recipeSteps = recipeSteps;

        const recipeStepCollection: IRecipeStep[] = [{ id: 22033 }];
        spyOn(recipeStepService, 'query').and.returnValue(of(new HttpResponse({ body: recipeStepCollection })));
        const additionalRecipeSteps = [...recipeSteps];
        const expectedCollection: IRecipeStep[] = [...additionalRecipeSteps, ...recipeStepCollection];
        spyOn(recipeStepService, 'addRecipeStepToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ recipe });
        comp.ngOnInit();

        expect(recipeStepService.query).toHaveBeenCalled();
        expect(recipeStepService.addRecipeStepToCollectionIfMissing).toHaveBeenCalledWith(recipeStepCollection, ...additionalRecipeSteps);
        expect(comp.recipeStepsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const recipe: IRecipe = { id: 456 };
        const tags: ITag = { id: 51406 };
        recipe.tags = [tags];
        const ingredients: IIngredient = { id: 46253 };
        recipe.ingredients = [ingredients];
        const recipePictures: IReceipePicture = { id: 19163 };
        recipe.recipePictures = [recipePictures];
        const recipeSteps: IRecipeStep = { id: 7488 };
        recipe.recipeSteps = [recipeSteps];

        activatedRoute.data = of({ recipe });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(recipe));
        expect(comp.tagsSharedCollection).toContain(tags);
        expect(comp.ingredientsSharedCollection).toContain(ingredients);
        expect(comp.receipePicturesSharedCollection).toContain(recipePictures);
        expect(comp.recipeStepsSharedCollection).toContain(recipeSteps);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const recipe = { id: 123 };
        spyOn(recipeService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ recipe });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: recipe }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(recipeService.update).toHaveBeenCalledWith(recipe);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const recipe = new Recipe();
        spyOn(recipeService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ recipe });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: recipe }));
        saveSubject.complete();

        // THEN
        expect(recipeService.create).toHaveBeenCalledWith(recipe);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const recipe = { id: 123 };
        spyOn(recipeService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ recipe });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(recipeService.update).toHaveBeenCalledWith(recipe);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackTagById', () => {
        it('Should return tracked Tag primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackTagById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackIngredientById', () => {
        it('Should return tracked Ingredient primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackIngredientById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackReceipePictureById', () => {
        it('Should return tracked ReceipePicture primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackReceipePictureById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackRecipeStepById', () => {
        it('Should return tracked RecipeStep primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackRecipeStepById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });

    describe('Getting selected relationships', () => {
      describe('getSelectedTag', () => {
        it('Should return option if no Tag is selected', () => {
          const option = { id: 123 };
          const result = comp.getSelectedTag(option);
          expect(result === option).toEqual(true);
        });

        it('Should return selected Tag for according option', () => {
          const option = { id: 123 };
          const selected = { id: 123 };
          const selected2 = { id: 456 };
          const result = comp.getSelectedTag(option, [selected2, selected]);
          expect(result === selected).toEqual(true);
          expect(result === selected2).toEqual(false);
          expect(result === option).toEqual(false);
        });

        it('Should return option if this Tag is not selected', () => {
          const option = { id: 123 };
          const selected = { id: 456 };
          const result = comp.getSelectedTag(option, [selected]);
          expect(result === option).toEqual(true);
          expect(result === selected).toEqual(false);
        });
      });

      describe('getSelectedIngredient', () => {
        it('Should return option if no Ingredient is selected', () => {
          const option = { id: 123 };
          const result = comp.getSelectedIngredient(option);
          expect(result === option).toEqual(true);
        });

        it('Should return selected Ingredient for according option', () => {
          const option = { id: 123 };
          const selected = { id: 123 };
          const selected2 = { id: 456 };
          const result = comp.getSelectedIngredient(option, [selected2, selected]);
          expect(result === selected).toEqual(true);
          expect(result === selected2).toEqual(false);
          expect(result === option).toEqual(false);
        });

        it('Should return option if this Ingredient is not selected', () => {
          const option = { id: 123 };
          const selected = { id: 456 };
          const result = comp.getSelectedIngredient(option, [selected]);
          expect(result === option).toEqual(true);
          expect(result === selected).toEqual(false);
        });
      });

      describe('getSelectedReceipePicture', () => {
        it('Should return option if no ReceipePicture is selected', () => {
          const option = { id: 123 };
          const result = comp.getSelectedReceipePicture(option);
          expect(result === option).toEqual(true);
        });

        it('Should return selected ReceipePicture for according option', () => {
          const option = { id: 123 };
          const selected = { id: 123 };
          const selected2 = { id: 456 };
          const result = comp.getSelectedReceipePicture(option, [selected2, selected]);
          expect(result === selected).toEqual(true);
          expect(result === selected2).toEqual(false);
          expect(result === option).toEqual(false);
        });

        it('Should return option if this ReceipePicture is not selected', () => {
          const option = { id: 123 };
          const selected = { id: 456 };
          const result = comp.getSelectedReceipePicture(option, [selected]);
          expect(result === option).toEqual(true);
          expect(result === selected).toEqual(false);
        });
      });

      describe('getSelectedRecipeStep', () => {
        it('Should return option if no RecipeStep is selected', () => {
          const option = { id: 123 };
          const result = comp.getSelectedRecipeStep(option);
          expect(result === option).toEqual(true);
        });

        it('Should return selected RecipeStep for according option', () => {
          const option = { id: 123 };
          const selected = { id: 123 };
          const selected2 = { id: 456 };
          const result = comp.getSelectedRecipeStep(option, [selected2, selected]);
          expect(result === selected).toEqual(true);
          expect(result === selected2).toEqual(false);
          expect(result === option).toEqual(false);
        });

        it('Should return option if this RecipeStep is not selected', () => {
          const option = { id: 123 };
          const selected = { id: 456 };
          const result = comp.getSelectedRecipeStep(option, [selected]);
          expect(result === option).toEqual(true);
          expect(result === selected).toEqual(false);
        });
      });
    });
  });
});
