import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IRecipe, Recipe } from '../recipe.model';
import { RecipeService } from '../service/recipe.service';
import { ITag } from 'app/entities/mymenums/tag/tag.model';
import { TagService } from 'app/entities/mymenums/tag/service/tag.service';
import { IIngredient } from 'app/entities/mymenums/ingredient/ingredient.model';
import { IngredientService } from 'app/entities/mymenums/ingredient/service/ingredient.service';
import { IReceipePicture } from 'app/entities/mymenums/receipe-picture/receipe-picture.model';
import { ReceipePictureService } from 'app/entities/mymenums/receipe-picture/service/receipe-picture.service';
import { IRecipeStep } from 'app/entities/mymenums/recipe-step/recipe-step.model';
import { RecipeStepService } from 'app/entities/mymenums/recipe-step/service/recipe-step.service';

@Component({
  selector: 'jhi-recipe-update',
  templateUrl: './recipe-update.component.html',
})
export class RecipeUpdateComponent implements OnInit {
  isSaving = false;

  tagsSharedCollection: ITag[] = [];
  ingredientsSharedCollection: IIngredient[] = [];
  receipePicturesSharedCollection: IReceipePicture[] = [];
  recipeStepsSharedCollection: IRecipeStep[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(400)]],
    description: [null, [Validators.maxLength(10000)]],
    preparationDuration: [null, [Validators.min(0)]],
    restDuraction: [null, [Validators.min(0)]],
    cookingDuration: [null, [Validators.min(0)]],
    winterPreferrency: [],
    springPreferrency: [],
    summerPreferrency: [],
    autumnPreferrency: [],
    numberOfPeople: [null, [Validators.min(1)]],
    recipeType: [null, [Validators.required]],
    recipeUrl: [null, [Validators.minLength(5), Validators.maxLength(200)]],
    shortSteps: [null, [Validators.maxLength(40000)]],
    isShortDescRecipe: [null, [Validators.required]],
    tags: [],
    ingredients: [],
    recipePictures: [],
    recipeSteps: [],
  });

  constructor(
    protected recipeService: RecipeService,
    protected tagService: TagService,
    protected ingredientService: IngredientService,
    protected receipePictureService: ReceipePictureService,
    protected recipeStepService: RecipeStepService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recipe }) => {
      this.updateForm(recipe);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const recipe = this.createFromForm();
    if (recipe.id !== undefined) {
      this.subscribeToSaveResponse(this.recipeService.update(recipe));
    } else {
      this.subscribeToSaveResponse(this.recipeService.create(recipe));
    }
  }

  trackTagById(index: number, item: ITag): number {
    return item.id!;
  }

  trackIngredientById(index: number, item: IIngredient): number {
    return item.id!;
  }

  trackReceipePictureById(index: number, item: IReceipePicture): number {
    return item.id!;
  }

  trackRecipeStepById(index: number, item: IRecipeStep): number {
    return item.id!;
  }

  getSelectedTag(option: ITag, selectedVals?: ITag[]): ITag {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  getSelectedIngredient(option: IIngredient, selectedVals?: IIngredient[]): IIngredient {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  getSelectedReceipePicture(option: IReceipePicture, selectedVals?: IReceipePicture[]): IReceipePicture {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  getSelectedRecipeStep(option: IRecipeStep, selectedVals?: IRecipeStep[]): IRecipeStep {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRecipe>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(recipe: IRecipe): void {
    this.editForm.patchValue({
      id: recipe.id,
      name: recipe.name,
      description: recipe.description,
      preparationDuration: recipe.preparationDuration,
      restDuraction: recipe.restDuraction,
      cookingDuration: recipe.cookingDuration,
      winterPreferrency: recipe.winterPreferrency,
      springPreferrency: recipe.springPreferrency,
      summerPreferrency: recipe.summerPreferrency,
      autumnPreferrency: recipe.autumnPreferrency,
      numberOfPeople: recipe.numberOfPeople,
      recipeType: recipe.recipeType,
      recipeUrl: recipe.recipeUrl,
      shortSteps: recipe.shortSteps,
      isShortDescRecipe: recipe.isShortDescRecipe,
      tags: recipe.tags,
      ingredients: recipe.ingredients,
      recipePictures: recipe.recipePictures,
      recipeSteps: recipe.recipeSteps,
    });

    this.tagsSharedCollection = this.tagService.addTagToCollectionIfMissing(this.tagsSharedCollection, ...(recipe.tags ?? []));
    this.ingredientsSharedCollection = this.ingredientService.addIngredientToCollectionIfMissing(
      this.ingredientsSharedCollection,
      ...(recipe.ingredients ?? [])
    );
    this.receipePicturesSharedCollection = this.receipePictureService.addReceipePictureToCollectionIfMissing(
      this.receipePicturesSharedCollection,
      ...(recipe.recipePictures ?? [])
    );
    this.recipeStepsSharedCollection = this.recipeStepService.addRecipeStepToCollectionIfMissing(
      this.recipeStepsSharedCollection,
      ...(recipe.recipeSteps ?? [])
    );
  }

  protected loadRelationshipsOptions(): void {
    this.tagService
      .query()
      .pipe(map((res: HttpResponse<ITag[]>) => res.body ?? []))
      .pipe(map((tags: ITag[]) => this.tagService.addTagToCollectionIfMissing(tags, ...(this.editForm.get('tags')!.value ?? []))))
      .subscribe((tags: ITag[]) => (this.tagsSharedCollection = tags));

    this.ingredientService
      .query()
      .pipe(map((res: HttpResponse<IIngredient[]>) => res.body ?? []))
      .pipe(
        map((ingredients: IIngredient[]) =>
          this.ingredientService.addIngredientToCollectionIfMissing(ingredients, ...(this.editForm.get('ingredients')!.value ?? []))
        )
      )
      .subscribe((ingredients: IIngredient[]) => (this.ingredientsSharedCollection = ingredients));

    this.receipePictureService
      .query()
      .pipe(map((res: HttpResponse<IReceipePicture[]>) => res.body ?? []))
      .pipe(
        map((receipePictures: IReceipePicture[]) =>
          this.receipePictureService.addReceipePictureToCollectionIfMissing(
            receipePictures,
            ...(this.editForm.get('recipePictures')!.value ?? [])
          )
        )
      )
      .subscribe((receipePictures: IReceipePicture[]) => (this.receipePicturesSharedCollection = receipePictures));

    this.recipeStepService
      .query()
      .pipe(map((res: HttpResponse<IRecipeStep[]>) => res.body ?? []))
      .pipe(
        map((recipeSteps: IRecipeStep[]) =>
          this.recipeStepService.addRecipeStepToCollectionIfMissing(recipeSteps, ...(this.editForm.get('recipeSteps')!.value ?? []))
        )
      )
      .subscribe((recipeSteps: IRecipeStep[]) => (this.recipeStepsSharedCollection = recipeSteps));
  }

  protected createFromForm(): IRecipe {
    return {
      ...new Recipe(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
      preparationDuration: this.editForm.get(['preparationDuration'])!.value,
      restDuraction: this.editForm.get(['restDuraction'])!.value,
      cookingDuration: this.editForm.get(['cookingDuration'])!.value,
      winterPreferrency: this.editForm.get(['winterPreferrency'])!.value,
      springPreferrency: this.editForm.get(['springPreferrency'])!.value,
      summerPreferrency: this.editForm.get(['summerPreferrency'])!.value,
      autumnPreferrency: this.editForm.get(['autumnPreferrency'])!.value,
      numberOfPeople: this.editForm.get(['numberOfPeople'])!.value,
      recipeType: this.editForm.get(['recipeType'])!.value,
      recipeUrl: this.editForm.get(['recipeUrl'])!.value,
      shortSteps: this.editForm.get(['shortSteps'])!.value,
      isShortDescRecipe: this.editForm.get(['isShortDescRecipe'])!.value,
      tags: this.editForm.get(['tags'])!.value,
      ingredients: this.editForm.get(['ingredients'])!.value,
      recipePictures: this.editForm.get(['recipePictures'])!.value,
      recipeSteps: this.editForm.get(['recipeSteps'])!.value,
    };
  }
}
