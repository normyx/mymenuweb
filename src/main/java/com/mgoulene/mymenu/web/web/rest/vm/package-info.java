/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mgoulene.mymenu.web.web.rest.vm;
