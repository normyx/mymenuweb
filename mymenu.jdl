application {
  config {
    applicationType microservice
    authenticationType oauth2
    baseName mymenums
    blueprints []
    buildTool maven
    cacheProvider hazelcast
    clientPackageManager npm
    databaseType sql
    devDatabaseType h2Disk
    dtoSuffix DTO
    enableHibernateCache true
    enableSwaggerCodegen false
    enableTranslation true
    jhiPrefix jhi
    jhipsterVersion "7.0.0"
    jwtSecretKey "YmY3ZjcxNjZiYTJmMmU5MDVhMmI4ZmIxYjgzYzk3MmQyYjMxMjRlZTlkMDFlMjdhM2Y2OTVhYWQ0MmYxZjViZDYwYjk3N2EwODM5NDI0ZTM5ZWMwN2M2OWRmNjdjZjQ0M2UwYzVlYmE1Njg3YTM4OGZhYWI1YzE0NmU3NDcxNWM="
    languages [fr, en]
    messageBroker false
    nativeLanguage fr
    otherModules []
    packageName com.mgoulene.mymenu.ms
    prodDatabaseType postgresql
    reactive false
    searchEngine false
    serverPort 8081
    serviceDiscoveryType eureka
    skipClient true
    skipUserManagement true
    testFrameworks [cypress, gatling, cucumber]
    websocket false
  }
  entities *
}

application {
  config {
    applicationType gateway
    authenticationType oauth2
    baseName mymenuweb
    blueprints []
    buildTool maven
    cacheProvider no
    clientFramework angularX
    clientPackageManager npm
    clientTheme sketchy
    clientThemeVariant dark
    databaseType sql
    devDatabaseType h2Disk
    dtoSuffix DTO
    enableHibernateCache false
    enableSwaggerCodegen false
    enableTranslation true
    jhiPrefix jhi
    jhipsterVersion "7.0.0"
    languages [fr, en]
    messageBroker false
    nativeLanguage fr
    otherModules []
    packageName com.mgoulene.mymenu.web
    prodDatabaseType postgresql
    reactive true
    searchEngine false
    serverPort 8080
    serviceDiscoveryType eureka
    skipClient false
    skipServer false
    skipUserManagement true
    testFrameworks [cypress, gatling, cucumber]
    websocket false
    withAdminUi true
  }
  entities *
}




enum QuantityType {
  QUANTITY,
  G,
  KG,
  L,
  ML,
  SOUPSPOON,
  TEASPON,
  PINCH
}

enum PreferredSeason {
  TRUE,
  FALSE
}

enum RecipeType {
  STARTER,
  MAIN,
  DESSERT
}

entity Menu {
  date LocalDate required,
  userId String required
}

/**
 * The Recipe entity.
 * @author A true hipster
 */
entity Recipe {
  /** fieldName */
  name String required maxlength(400),
  description String maxlength(10000),
  preparationDuration Integer min(0),
  restDuraction Integer min(0),
  cookingDuration Integer min(0),
  winterPreferrency PreferredSeason,
  springPreferrency PreferredSeason,
  summerPreferrency PreferredSeason,
  autumnPreferrency PreferredSeason,
  numberOfPeople Integer min(1),
  recipeType RecipeType required
}

entity Tag {
  label String required minlength(4) maxlength(40)
}

entity RecipeStep {
  stepNum Integer required min(1),
  stepDescription String maxlength(4000)
}

entity ReceipePicture {
  picture ImageBlob required,
  label String maxlength(400),
  order Integer required min(1)
}

/**
 * The Ingredient entity.
 * @author A true hipster
 */
entity Ingredient {
  quantity Float,
  quantityType QuantityType

}

/**
 * The Sheld entity.
 * @author A true hipster
 */
entity Sheld {
  /** name */
  name String required maxlength(200)
}

/**
 * The Product entity.
 * @author A true hipster
 */
entity Product {
  /** name */
  name String required maxlength(200)
  picture ImageBlob
}




relationship ManyToOne {
  Product{sheld(name)} to Sheld,
  Ingredient{product(name)} to Product,
  Menu{starter(name)} to Recipe,
  Menu{main(name)} to Recipe,
  Menu{dessert(name)} to Recipe,
}


relationship ManyToMany {
  Recipe{tag(label)} to Tag{recipe}
  Recipe{ingredient(quantityType)} to Ingredient{recipe}
  Recipe{recipePicture(label)} to ReceipePicture{recipe}
  Recipe{recipeStep(stepDescription)} to RecipeStep{recipe}
}


dto * with mapstruct
paginate * with infinite-scroll
service * with serviceClass
filter *
microservice * with mymenums
